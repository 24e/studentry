<?php

include "connessione.php";
$json = array();
$json2 = array();

if (!$result = $conn->query("SELECT * FROM registrazione
              JOIN alunno ON alunno.ID_alunno = registrazione.FK_alunno
              WHERE datareg = CURDATE()

              and orario_uscita IS NULL
              and alunno.FK_classe = 1")) {
  echo "Errore nella query " . $connessione->error . ".";
  exit();
}else{

  // conteggio dei record
  if($result->num_rows > 0) {
    // conteggio dei record restituiti dalla query
    while($row = $result->fetch_assoc())
    {
      
      array_push($json , array('id'=>$row['ID_alunno'],'nome'=>$row['nome'],'cognome'=>$row['cognome'],'orario_entrata'=>$row['orario_entrata']));
    }
    /*echo json_encode(array("result"=>$json));
    // liberazione delle risorse occupate dal risultato
    $result->close();*/
  }
}

if (!$result = $conn->query("SELECT * FROM
                (SELECT * FROM registrazione
                  WHERE datareg = CURDATE()) tmp RIGHT JOIN alunno
                ON alunno.ID_alunno = tmp.FK_alunno
                WHERE (orario_uscita IS NOT NULL or orario_entrata IS NULL) and alunno.FK_classe = 1")) 
{
  echo "Errore nella query " . $connessione->error . ".";
  exit();
}else{

  // conteggio dei record
  if($result->num_rows > 0) {
    // conteggio dei record restituiti dalla query
    while($row = $result->fetch_assoc())
    {
      
      array_push($json2 , array('id'=>$row['ID_alunno'],'nome'=>$row['nome'],'cognome'=>$row['cognome'],'orario_uscita'=>$row['orario_uscita']));
    }
    /*echo json_encode(array("result2"=>$json2));
    // liberazione delle risorse occupate dal risultato
    $result->close();*/
  }
}
echo json_encode(array("result"=>$json,"result2"=>$json2));
// chiusura della connessione
$conn->close();
?>