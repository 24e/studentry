package com.slack.e420.scuola;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText usernamel =(EditText) findViewById(R.id.Usernamel);
        final EditText passwordl =(EditText) findViewById(R.id.Passwordl);
        final TextView link =(TextView) findViewById(R.id.Link);

        final Button login =(Button) findViewById(R.id.Login);

        link.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Intent regintent = new Intent(LoginActivity.this,RegisterActivity.class);
                LoginActivity.this.startActivity(regintent);
            }
        });

        login.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                final String username = usernamel.getText().toString();
                final String password = passwordl.getText().toString();

                Response.Listener<String> LogListener=new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if(success){

                                Intent intent= new Intent(LoginActivity.this,MainActivity.class);
                                LoginActivity.this.startActivity(intent);
                                usernamel.setText("");
                                passwordl.setText("");

                            }else{
                                Toast.makeText(LoginActivity.this, "Errore di connessione riprova...", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };

                LoginRequest LogReq = new LoginRequest(username,password,LogListener);
                RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                queue.add(LogReq);
            }
        });
    }
}
