package com.slack.e420.scuola;

/**
 * Created by tarikl on 24/04/17.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.support.v7.widget.DefaultItemAnimator;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import static android.R.attr.data;
import static com.slack.e420.scuola.Controller.TAG;


public class MainActivity extends Activity {

    private  RecyclerView.Adapter adapter, adapter2;
    private RecyclerView.LayoutManager layoutManager, layoutManager2;
    private  RecyclerView recyclerView, recyclerView2;

    private static final String url = "https://tariklyacoubi.000webhostapp.com";
    public static List<DataSet> list = new ArrayList<>();
    public static List<DataSet> list2 = new ArrayList<>();



    @Override
    protected void onStart() {
        super.onStart();



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view2);
        recyclerView.setHasFixedSize(true);
        recyclerView2 = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView2.setHasFixedSize(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.clear();
                list2.clear();
                updateData();
                Toast.makeText(MainActivity.this, "Ricerca aggiornamenti...", Toast.LENGTH_SHORT).show();
            }
        });



        layoutManager = new LinearLayoutManager(this);
        layoutManager2 = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView2.setLayoutManager(layoutManager2);
        adapter = new CustomAdapter(getApplicationContext(), list);
        adapter2 = new CustomAdapter(getApplicationContext(), list2);
        recyclerView.setAdapter(adapter2);
        recyclerView2.setAdapter(adapter);

        adapter.notifyDataSetChanged();
        adapter2.notifyDataSetChanged();
        updateData();


    }

    private void updateData() {

        RequestQueue requestQueue= Volley.newRequestQueue(this);

        StringRequest jsonreq =new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    JSONObject jsonObject = new JSONObject("" + response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    for (int i = 0; i < jsonArray.length(); i++) {


                        JSONObject obj = jsonArray.getJSONObject(i);
                        DataSet dataSet = new DataSet();
                        dataSet.setID(obj.getInt("id"));
                        dataSet.setName(obj.getString("nome"));
                        dataSet.setSurname(obj.getString("cognome"));
                        dataSet.setOrarioEntrata(obj.getString("orario_entrata"));
                        list.add(dataSet);
                        adapter.notifyDataSetChanged();
                    }
                    //
                    JSONArray jsonArray2 = jsonObject.getJSONArray("result2");
                    for (int i = 0; i < jsonArray2.length(); i++) {


                        JSONObject obj = jsonArray2.getJSONObject(i);
                        DataSet dataSet = new DataSet();
                        dataSet.setID(obj.getInt("id"));
                        dataSet.setName(obj.getString("nome"));
                        dataSet.setSurname(obj.getString("cognome"));
                        dataSet.setOrarioUscita(obj.getString("orario_uscita"));
                        list2.add(dataSet);
                        adapter2.notifyDataSetChanged();
                    }


                    //

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder add = new AlertDialog.Builder(MainActivity.this);
                add.setMessage(error.getMessage()).setCancelable(true);
                Log.d(TAG, "onErrorResponse: "+error.getMessage());
                AlertDialog alert = add.create();
                alert.setTitle("Error!!!");
                alert.show();
                Toast.makeText(MainActivity.this, ""+error, Toast.LENGTH_SHORT).show();
                NetworkResponse errorRes = error.networkResponse;
                String stringData = "";
                try {
                    if (errorRes != null && errorRes.data != null) {
                        stringData = new String(errorRes.data, "UTF-8");
                    }
                    Log.e("Error", stringData);
                }catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        requestQueue.add(jsonreq);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.add_item) {
            //check if any items to add
            if (removedItems.size() != 0) {
                addRemovedItemToList();
            } else {
                Toast.makeText(this, "Nothing to add", Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    private void addRemovedItemToList() {

    }*/

}

