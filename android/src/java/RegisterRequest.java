package com.slack.e420.scuola;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tarikl on 08/06/17.
 */

public class RegisterRequest extends StringRequest{

    private static final String RegUrl="https://tariklyacoubi.000webhostapp.com/Register.php";
    private Map<String, String> params;


    public RegisterRequest(String name, String username, String password, Response.Listener<String> listener){

        super(Method.POST,RegUrl,listener,null);
        params= new HashMap<>();
        params.put("name",name);
        params.put("username",username);
        params.put("password",password);

    }

        @Override
        public Map<String, String> getParams()  {

            return params;
        }

}