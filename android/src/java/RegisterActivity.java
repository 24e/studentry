package com.slack.e420.scuola;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import static com.slack.e420.scuola.R.styleable.AlertDialog;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText tname =(EditText) findViewById(R.id.Name);
        final EditText tusername =(EditText) findViewById(R.id.Username);
        final EditText tpassword =(EditText) findViewById(R.id.Password);

        final Button register =(Button) findViewById(R.id.Register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String name = tname.getText().toString();
                final String username = tusername.getText().toString();
                final String password = tpassword.getText().toString();

                Response.Listener<String> Rslistener = new Response.Listener<String>(){

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if(success){
                                Intent intent= new Intent(RegisterActivity.this,LoginActivity.class);
                                RegisterActivity.this.startActivity(intent);
                            }else{
                                Toast.makeText(RegisterActivity.this, "Errore di connessione riprova...", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };

                RegisterRequest regrq = new RegisterRequest(name,username,password,Rslistener);
                RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
                queue.add(regrq);
            }
        });
    }
}
