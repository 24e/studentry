package com.slack.e420.scuola;

/**
 * Created by tarikl on 24/04/17.
 */


public class DataSet {

    private String name, surname,orario_entrata,orario_uscita;
    private int id;

    public void setID(int id) {
        this.id = id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setOrarioEntrata(String orario_entrata) {
        this.orario_entrata = orario_entrata;
    }
    public void setOrarioUscita(String orario_uscita) {
        this.orario_uscita = orario_uscita;
    }

    public String getSurname() {return surname;}
    public String getName() {return name;}
    public String getOrarioEntrata() {return orario_entrata;}
    public String getOrarioUscita() {
        return orario_uscita;
    }
    public int getId() {
        return id;
    }
}