package com.slack.e420.scuola;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.slack.e420.scuola.Controller.TAG;
import static com.slack.e420.scuola.R.styleable.RecyclerView;


/**
 * Created by tarikl on 24/04/17.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private final Context context;
    private final List<DataSet> list;

    //private LayoutInflater inflater;


    public class MyViewHolder extends  RecyclerView.ViewHolder{

        private TextView name;
        private TextView time;
        private CardView card;
        private Button approva;
        Context context;


        public MyViewHolder(View itemView) {

            super(itemView);

            card = (CardView) itemView.findViewById(R.id.card_view);
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.time);
            approva = (Button) itemView.findViewById(R.id.button);
        }


    }


    public CustomAdapter(Context applicationContext, List<DataSet> list){
        super();
        this.context = applicationContext;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row, parent, false);
        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DataSet dataset = list.get(position);



        holder.approva.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {



                int id = dataset.getId();


                Response.Listener<String> listener = new Response.Listener<String>(){

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if(success){
                                list.remove( holder.getAdapterPosition());
                                notifyDataSetChanged();
                            }else{

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };
                ApprovalRequest areq = new ApprovalRequest(id,listener);
                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.add(areq);


            }
        });
        
        holder.name.setText(dataset.getSurname() +" "+dataset.getName());
        holder.time.setText(dataset.getOrarioEntrata());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    
    
    /*
    public void updateData(ArrayList<ViewModel> viewModels) {
        items.clear();
        items.addAll(viewModels);
        notifyDataSetChanged();
    }*/


}