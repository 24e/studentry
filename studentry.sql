-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 19, 2017 at 03:46 PM
-- Server version: 5.5.55-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `studentry`
--

-- --------------------------------------------------------

--
-- Table structure for table `alunno`
--

CREATE TABLE IF NOT EXISTS `alunno` (
`ID_alunno` int(11) NOT NULL,
  `nome` varchar(20) DEFAULT NULL,
  `numerotessera` varchar(12) DEFAULT NULL,
  `telegram_username` varchar(50) NOT NULL,
  `chat_id` varchar(10) NOT NULL,
  `FK_classe` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alunno`
--

INSERT INTO `alunno` (`ID_alunno`, `nome`, `numerotessera`, `telegram_username`, `chat_id`, `FK_classe`) VALUES
(1, 'alunno01', '14427134122', 'n_possamai', '19521412', 1),
(2, 'alunno02', '80239123122', 'n_possamai', '19521412', 1),
(3, 'alunno03', '32240124122', 'Multi0371', '386548732', 2),
(4, 'alunno04', '444', 'Multi0371', '', 2),
(5, 'alunno05', '32113129122', 'tarkdemi', '189922137', 2),
(6, 'alunno06', '666', '', '', 2),
(7, 'alunno07', '777', '', '', 1),
(8, 'alunno08', '888', '', '', 1),
(9, 'alunno09', '999', '', '', 3),
(10, 'alunno10', '101010', '', '', 3),
(11, 'alunno11', '111111', '', '', 3),
(12, 'alunno12', '121212', '', '', 4),
(13, 'alunno13', '131313', '', '', 5),
(14, 'alunno14', '141414', '', '', 4),
(15, 'alunno15', '151515', '', '', 5);

-- --------------------------------------------------------

--
-- Table structure for table `assenza`
--

CREATE TABLE IF NOT EXISTS `assenza` (
`ID` int(11) NOT NULL,
  `FK_alunno` int(11) DEFAULT NULL,
  `FK_calendario` int(11) DEFAULT NULL,
  `giustificata` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assenza`
--

INSERT INTO `assenza` (`ID`, `FK_alunno`, `FK_calendario`, `giustificata`) VALUES
(8, 1, 2, '2017-05-08'),
(9, 2, 4, '2017-05-08'),
(10, 7, 4, '2017-05-08'),
(11, 4, 4, '2017-05-08'),
(12, 5, 4, '2017-05-12'),
(13, 6, 4, '2017-05-12'),
(14, 1, 3, '2017-05-12'),
(15, 2, 4, '2017-05-12'),
(16, 7, 4, '2017-05-12'),
(17, 1, 4, '2017-05-12'),
(18, 5, 4, '2017-05-12'),
(19, 1, 6, '2017-05-12'),
(20, 5, 3, NULL),
(26, 1, 8, NULL),
(27, 2, 9, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `calendario`
--

CREATE TABLE IF NOT EXISTS `calendario` (
`ID` int(11) NOT NULL,
  `giorno` date DEFAULT NULL,
  `valore` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `calendario`
--

INSERT INTO `calendario` (`ID`, `giorno`, `valore`) VALUES
(1, '2017-05-01', 'Festa dei lavoratori'),
(2, '2017-05-02', 'scolastico'),
(3, '2017-05-03', 'scolastico'),
(4, '2017-05-04', 'scolastico'),
(5, '2017-05-05', 'scolastico'),
(6, '2017-05-06', 'scolastico'),
(7, '2017-05-07', 'Domenica'),
(8, '2017-06-09', 'scolastico'),
(9, '2017-06-19', 'scolastico');

-- --------------------------------------------------------

--
-- Table structure for table `classe`
--

CREATE TABLE IF NOT EXISTS `classe` (
`ID_classe` int(11) NOT NULL,
  `nome` varchar(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `classe`
--

INSERT INTO `classe` (`ID_classe`, `nome`) VALUES
(1, '5AIA'),
(2, '5AEC'),
(3, '5AAM'),
(4, '5ACA'),
(5, '5AET');

-- --------------------------------------------------------

--
-- Table structure for table `docente`
--

CREATE TABLE IF NOT EXISTS `docente` (
`ID_docente` int(11) NOT NULL,
  `nome` varchar(30) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `docente`
--

INSERT INTO `docente` (`ID_docente`, `nome`, `username`, `password`) VALUES
(1, 'docente01', 'doc01', '111'),
(2, 'docente02', 'doc02', '222'),
(3, 'docente03', 'doc03', '333'),
(4, 'docente04', 'doc04', '444'),
(5, 'docente05', 'doc05', '555'),
(10, 'docente06', 'doc06', '$2y$10$30ivIAUTzFCh0g4D41nwX.hmgpOVptjelggYGjb2EvJUuBiJOEb7C'),
(11, 'doc08', 'doc08', '$2y$10$b1pqEMiG3YOS1W1J9glhFOiJr2YkBdfvqR0uF8Z8iFdmWO7kGgyUm'),
(12, 'docente07', 'doc07', '$2y$10$etNKvOFdFqoLknHJpVImxeStnC0/CXGsZbvCAiV0UnzODHKqxDhBq');

-- --------------------------------------------------------

--
-- Table structure for table `giorno`
--

CREATE TABLE IF NOT EXISTS `giorno` (
`ID` int(11) NOT NULL,
  `nome` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `giorno`
--

INSERT INTO `giorno` (`ID`, `nome`) VALUES
(1, 'Lunedì'),
(2, 'Martedì'),
(3, 'Mercoledì'),
(4, 'Giovedì'),
(5, 'Venerdì'),
(6, 'Sabato');

-- --------------------------------------------------------

--
-- Table structure for table `lezione`
--

CREATE TABLE IF NOT EXISTS `lezione` (
`ID` int(11) NOT NULL,
  `FK_orario` int(11) NOT NULL,
  `FK_docente` int(11) NOT NULL,
  `FK_classe` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lezione`
--

INSERT INTO `lezione` (`ID`, `FK_orario`, `FK_docente`, `FK_classe`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 2, 1),
(4, 4, 2, 1),
(5, 5, 3, 1),
(6, 6, 4, 1),
(7, 1, 2, 2),
(8, 2, 3, 2),
(9, 3, 3, 2),
(10, 4, 4, 2),
(11, 5, 4, 2),
(12, 6, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ora`
--

CREATE TABLE IF NOT EXISTS `ora` (
`ID` int(11) NOT NULL,
  `ora_inizio` time DEFAULT NULL,
  `ora_fine` time DEFAULT NULL,
  `nome_ora` char(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ora`
--

INSERT INTO `ora` (`ID`, `ora_inizio`, `ora_fine`, `nome_ora`) VALUES
(1, '07:55:00', '08:45:00', '1'),
(2, '08:45:00', '09:35:00', '2'),
(3, '09:35:00', '10:25:00', '3'),
(4, '10:35:00', '11:25:00', '4'),
(5, '11:25:00', '12:15:00', '5'),
(6, '12:15:00', '13:05:00', '6'),
(7, '07:55:00', '08:55:00', '1'),
(8, '08:55:00', '09:55:00', '2'),
(9, '09:55:00', '10:55:00', '3'),
(10, '11:05:00', '12:05:00', '4'),
(11, '12:05:00', '13:05:00', '5');

-- --------------------------------------------------------

--
-- Table structure for table `orario`
--

CREATE TABLE IF NOT EXISTS `orario` (
`ID` int(11) NOT NULL,
  `FK_giorno` int(11) NOT NULL,
  `FK_ora` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orario`
--

INSERT INTO `orario` (`ID`, `FK_giorno`, `FK_ora`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 2, 1),
(8, 2, 2),
(9, 2, 3),
(10, 2, 4),
(11, 2, 5),
(12, 2, 6),
(13, 3, 7),
(14, 3, 8),
(15, 3, 9),
(16, 3, 10),
(17, 3, 11),
(18, 4, 7),
(19, 4, 8),
(20, 4, 9),
(21, 4, 10),
(22, 4, 11),
(23, 5, 7),
(24, 5, 8),
(25, 5, 9),
(26, 5, 10),
(27, 5, 11),
(28, 6, 7),
(29, 6, 8),
(30, 6, 9),
(31, 6, 10),
(32, 6, 11);

-- --------------------------------------------------------

--
-- Table structure for table `registrazione`
--

CREATE TABLE IF NOT EXISTS `registrazione` (
`ID` int(11) NOT NULL,
  `datareg` date NOT NULL,
  `orario_entrata` time NOT NULL,
  `orario_uscita` time DEFAULT NULL,
  `FK_alunno` int(11) NOT NULL,
  `approve` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registrazione`
--

INSERT INTO `registrazione` (`ID`, `datareg`, `orario_entrata`, `orario_uscita`, `FK_alunno`, `approve`) VALUES
(206, '2017-06-19', '12:14:58', NULL, 2, 0),
(207, '2017-06-19', '12:15:00', NULL, 1, 0),
(209, '2017-06-19', '12:27:43', NULL, 3, 0),
(210, '2017-06-19', '12:28:05', NULL, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `uscita`
--

CREATE TABLE IF NOT EXISTS `uscita` (
`ID` int(11) NOT NULL,
  `ora` time DEFAULT NULL,
  `permesso_genitore` tinyint(1) NOT NULL,
  `FK_registrazione` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `utente_segreteria`
--

CREATE TABLE IF NOT EXISTS `utente_segreteria` (
`ID_utente` int(11) NOT NULL,
  `nome` varchar(30) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utente_segreteria`
--

INSERT INTO `utente_segreteria` (`ID_utente`, `nome`, `username`, `password`) VALUES
(2, 'segreteria01', 'segr01', '$2y$10$yM/isYvHGnMtfZ3Jd6Uciul.q4nxCapofT4xRRv8Koiyi9YG5ZQW.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alunno`
--
ALTER TABLE `alunno`
 ADD PRIMARY KEY (`ID_alunno`), ADD UNIQUE KEY `numerotessera` (`numerotessera`), ADD KEY `frequente` (`FK_classe`);

--
-- Indexes for table `assenza`
--
ALTER TABLE `assenza`
 ADD PRIMARY KEY (`ID`), ADD KEY `studente` (`FK_alunno`), ADD KEY `giorno` (`FK_calendario`);

--
-- Indexes for table `calendario`
--
ALTER TABLE `calendario`
 ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `giorno` (`giorno`);

--
-- Indexes for table `classe`
--
ALTER TABLE `classe`
 ADD PRIMARY KEY (`ID_classe`);

--
-- Indexes for table `docente`
--
ALTER TABLE `docente`
 ADD PRIMARY KEY (`ID_docente`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `giorno`
--
ALTER TABLE `giorno`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `lezione`
--
ALTER TABLE `lezione`
 ADD PRIMARY KEY (`ID`), ADD KEY `istruita` (`FK_classe`), ADD KEY `insegna` (`FK_docente`), ADD KEY `FK_giorno` (`FK_orario`);

--
-- Indexes for table `ora`
--
ALTER TABLE `ora`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `orario`
--
ALTER TABLE `orario`
 ADD PRIMARY KEY (`ID`), ADD KEY `appartiene` (`FK_ora`);

--
-- Indexes for table `registrazione`
--
ALTER TABLE `registrazione`
 ADD PRIMARY KEY (`ID`), ADD KEY `effettua` (`FK_alunno`);

--
-- Indexes for table `uscita`
--
ALTER TABLE `uscita`
 ADD PRIMARY KEY (`ID`), ADD KEY `genera` (`FK_registrazione`);

--
-- Indexes for table `utente_segreteria`
--
ALTER TABLE `utente_segreteria`
 ADD PRIMARY KEY (`ID_utente`), ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alunno`
--
ALTER TABLE `alunno`
MODIFY `ID_alunno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `assenza`
--
ALTER TABLE `assenza`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `calendario`
--
ALTER TABLE `calendario`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `classe`
--
ALTER TABLE `classe`
MODIFY `ID_classe` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `docente`
--
ALTER TABLE `docente`
MODIFY `ID_docente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `giorno`
--
ALTER TABLE `giorno`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lezione`
--
ALTER TABLE `lezione`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ora`
--
ALTER TABLE `ora`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `orario`
--
ALTER TABLE `orario`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `registrazione`
--
ALTER TABLE `registrazione`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=211;
--
-- AUTO_INCREMENT for table `uscita`
--
ALTER TABLE `uscita`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `utente_segreteria`
--
ALTER TABLE `utente_segreteria`
MODIFY `ID_utente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `alunno`
--
ALTER TABLE `alunno`
ADD CONSTRAINT `frequente` FOREIGN KEY (`FK_classe`) REFERENCES `classe` (`ID_classe`);

--
-- Constraints for table `assenza`
--
ALTER TABLE `assenza`
ADD CONSTRAINT `giorno` FOREIGN KEY (`FK_calendario`) REFERENCES `calendario` (`ID`),
ADD CONSTRAINT `studente` FOREIGN KEY (`FK_alunno`) REFERENCES `alunno` (`ID_alunno`);

--
-- Constraints for table `lezione`
--
ALTER TABLE `lezione`
ADD CONSTRAINT `insegna` FOREIGN KEY (`FK_docente`) REFERENCES `docente` (`ID_docente`),
ADD CONSTRAINT `istruita` FOREIGN KEY (`FK_classe`) REFERENCES `classe` (`ID_classe`),
ADD CONSTRAINT `lezione_ibfk_1` FOREIGN KEY (`FK_orario`) REFERENCES `orario` (`ID`);

--
-- Constraints for table `orario`
--
ALTER TABLE `orario`
ADD CONSTRAINT `appartiene` FOREIGN KEY (`FK_ora`) REFERENCES `ora` (`ID`);

--
-- Constraints for table `registrazione`
--
ALTER TABLE `registrazione`
ADD CONSTRAINT `effettua` FOREIGN KEY (`FK_alunno`) REFERENCES `alunno` (`ID_alunno`);

--
-- Constraints for table `uscita`
--
ALTER TABLE `uscita`
ADD CONSTRAINT `genera` FOREIGN KEY (`FK_registrazione`) REFERENCES `registrazione` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
