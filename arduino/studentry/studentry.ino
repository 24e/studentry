#include <Ethernet.h>
#include <SPI.h>

byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 // mac address dell'Ethernet Shield
};
IPAddress ip(192, 168, 0, 128); //indirizzo IP Arduino, se fallisce il DHCP

EthernetClient client; // istanzio la libreria EthernetClient


void setup() {
    
    // connessione attraverso DCHP 
    if (Ethernet.begin(mac) == 0) {
      Serial.println("Failed to configure Ethernet using DHCP");
      // connessione con indirizzo IP statico, se la configurazione con DHCP fallisce
      Ethernet.begin(mac, ip);
    }
        
    // si da all' Ethernet shield un secondo per inizializzarsi
    delay(1000);
    Serial.println("connecting...");
    printIPAddress();
    
}
    
void loop() {
    // viene richiamata la funzione per il mantenimento della connessione con DHCP
    maintain();
}

// funzione per in mantenimento dell'indirizzo fornito dal DHCP
void maintain(){
  switch (Ethernet.maintain()){
      case 1:
          //renewed fallito
            Serial.println("Error: renewed fail");
            break;
        
        case 2:
          //renewed avvenuto con successo
            Serial.println("Renewed success");
            //stampa l'indirizzo IP:
            printIPAddress();
            break;
            
        case 3:
          //rebind fallito
            Serial.println("Error: rebind fail");
            break;
            
        case 4:
          //rebind avvenuto con successo
            Serial.println("Rebind success");
            //stampa l'indirizzo IP:
            printIPAddress();
            break;
            
        default:
          //non succede nulla
            break;
  }       
}
    
void printIPAddress(){
  Serial.print("My IP address: ");
    for (byte thisByte = 0; thisByte < 4; thisByte++) {
        // stampa il valore di ogni byte dell'indirizzo IP
      Serial.print(Ethernet.localIP()[thisByte], DEC);
      Serial.print(".");
    }
  Serial.println();
}
