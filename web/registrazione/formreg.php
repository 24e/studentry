<?php

?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registrazione</title>

	<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="../lib/bootstrap/css/bootstrap-theme.min.css">
</head>

<body>
  <div class="container">
    <div class="jumbotron">
       <h1>Registrati</h1>
       <p>Inserimento dati per la registrazione</p>
    </div>
    <div class="page-header">
        <h1>Inserire i seguenti dati</h1>
    </div>

    <form method="post" action="insertreg.php">
        <div class="form-group row ">
            <label for="example-text-input" class="col-xs-2 col-form-label">Nome</label>
            <div class="col-xs-10">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </div>
                    <input class="form-control" type="text" name="nome" placeholder="Nome utente">
                </div>
            </div>
        </div>

        <div class="form-group row ">
          <label for="example-text-input" class="col-xs-2 col-form-label">Username</label>
          <div class="col-xs-10">
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-user"></i>
              </div>
              <input class="form-control" type="text" name="username" placeholder="username">
            </div>
          </div>
        </div>

        <div class="form-group row ">
          <label for="example-text-input" class="col-xs-2 col-form-label">Password</label>
          <div class="col-xs-10">
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-key"></i>
              </div>
              <input class="form-control" type="text" name="password" placeholder="password">
            </div>
          </div>
        </div>

        <p>Come ti stai registrando?</p>
        <select name="personale">
        <option value="docente">Docente</option>
        <option value="segretario">Segretario</option>
        <option value="amministratore">Amministratore</option>
        </select>

        <br>
        <br>

          <label for="example-text-input" class="col-xs-2 col-form-label"></label>
          <div class="col-xs-1">
            <div class="input-group">
              <button type="submit" class="btn btn-success">Invia</button>
            </div>
          </div>
          <div class="col-xs-2">
            <div class="input-group">
              <button type="reset" class="btn btn-warning">Cancella</button>
            </div>
          </div>
        </div>
    </form>
  </div>

  </body>
</html>


  </body>
</html>
