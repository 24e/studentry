<?php

/**
@file    index.php
@author  Nicholas Possamai, possa15@gmail.com
@version 1.0

Pagina madre per la parte docenti su cui vengono incluse tutte le altre.
*/

?>


<!-- JQUERY LOADER 2 -->
<script src="../lib/queryloader2/queryloader2.min.js" type="text/javascript"></script>
<script type="text/javascript">

      window.addEventListener('DOMContentLoaded', function() {
              loader();
      });

      /// @function loader()
      /// La funzione crea un overlay per nascondere il caricamento delle pagine e migliorare l'esperienza utente, utilizzando la libreria QueryLoader2
      function loader(){
        QueryLoader2(document.querySelector("body"), {
            barColor: "#efefef",
            backgroundColor: "#111",
            percentage: true,
            barHeight: 1,
            minimumTime: 200,
            fadeOutTime: 1000
        });
      }

</script>


<?php
/**
*
*/
session_start();
//include('../script/connessione.php');
//include("../login/session.php");
if(!isset($_SESSION['login_user'])){
   header("location:../login/login.php");
}/*else {
  include("strutture/index.html");
}
$classe = '';*/
  //echo "seesiom:".$_SESSION['login_user'];
 ?>

 <!DOCTYPE html>
 <html lang="it">
     <head>
         <meta charset="utf-8">
         <title>Docenti</title>
         <meta name="viewport" content="width=device-width, initial-scale=1">

         <!-- TEMA PRINCIPALE -->
         <link href="../lib/bootstrap/css/my.css" rel="stylesheet">
         <!-- TEMA BASE BOOTSTRAP -->
         <link href="../lib/bootstrap/css/bootstrap.css?version=1" rel="stylesheet">
         <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
         <!--FAFA ICON -->
         <link rel="stylesheet" href="../lib/font-awesome-4.7.0/css/font-awesome.min.css">
         <!-- JQUERY -->
         <script src="../lib/jquery/jquery-3.2.0.min.js"></script>
         <!-- JQUERY COOKIE -->
         <script src="../lib/js-cookie-master/src/js.cookie.js"></script>

         <script>
           $(document).ready(function() {
             $('#dashboard').load('strutture/home.php');
           });
         </script>
         <script type="text/javascript">

           /// caricamento della user-dashboard
           function dashload(id){
             loader();
             var classe = Cookies.get("classe");
             //classe active al div classe cliccato
             $('#nav').children().children().each(function (){
               if ($(this).attr('id')== id) {
                 $(this).attr('class','active');
               }else{
                 $(this).attr('class','');
               }
             });
             //caricamento pagina selezionata dashboard
             $('#dashboard').load($('#'+id).children().attr('href'));
           }

     </script>

     </head>

         <body class="home">


             <div class="container-fluid display-table">
                 <div class="row display-table-row">
                     <div class="col-md-2 col-sm-1 hidden-xs display-table-cell v-align box" id="navigation">
                         <div class="logo">
                             <a hef="home.html">
                                 <img src="../images/logo.png" alt="merkery_logo" class="img-responsive">
                                 <!--<img src="http://jskrishna.com/work/merkury/images/circle-logo.png" alt="merkery_logo" class="visible-xs visible-sm circle-logo">-->
                             </a>
                         </div>
                         <div id="nav" class="navi">
                             <ul>
                                 <li id="h" class="active" onclick="dashload(this.id)">
                                     <a href="strutture/home.php" onclick="return false"><i class="fa fa-home" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Home</span></a>
                                 </li>
                                 <li id="g" onclick="dashload(this.id)">
                                     <a href="strutture/giustificazioni.html" onclick="return false"><i class="fa fa-tasks" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Giustificazioni</span></a>
                                 </li>
                                 <li id="s" onclick="dashload(this.id)">
                                     <a href="strutture/storico.php" onclick="return false"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Storico Assenze</span></a>
                                 </li>
                                 <li id="p" onclick="dashload(this.id)">
                                     <a href="" onclick="return false"><i class="fa fa-user" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Profilo</span></a>
                                 </li>
                                 <li id="o" onclick="dashload(this.id)">
                                     <a href="#" onclick="return false"><i class="fa fa-calendar" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Orario</span></a>
                                 </li>
                                 <li id="i" onclick="dashload(this.id)">
                                     <a href="#" onclick="return false"><i class="fa fa-cog" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Impostazioni</span></a>
                                 </li>
                             </ul>
                         </div>
                     </div>
                     <div class="col-md-10 col-sm-11 display-table-cell v-align">
                         <!--<button type="button" class="slide-toggle">Slide Toggle</button> -->
                         <div class="row">
                             <header>
                                 <div class="col-md-7">
                                     <nav class="navbar-default pull-left">
                                         <div class="navbar-header">
                                             <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#side-menu" aria-expanded="false">
                                                 <span class="sr-only">Toggle navigation</span>
                                                 <span class="icon-bar"></span>
                                                 <span class="icon-bar"></span>
                                                 <span class="icon-bar"></span>
                                             </button>
                                         </div>
                                     </nav>
                                     <div class="search">
                                         <input type="text" placeholder="Search" id="search">
                                     </div>
                                 </div>
                                 <div class="col-md-5">
                                     <div class="header-rightside">
                                         <ul class="list-inline header-top pull-right">
                                             <!--<li class="hidden-xs"><a href="#" onclick="drag()" class="add-project" data-toggle="modal"><i class="fa fa-pencil" aria-hidden="true"></i>  Modifica</a></li>-->
                                             <li>
                                                 <a id="btn_modifica" href="#" onclick="change(this.id)" class="button mod" data-toggle="modal"><i class="fa fa-pencil" aria-hidden="true"></i> <span class="hidden-xs">Modifica</span></a>
                                                 <a id="btn_fatto" href="#" onclick="change(this.id)" class="button mod" data-toggle="modal"><i class="fa fa-check-circle" aria-hidden="true"></i>  <span class="hidden-xs">Fatto</span></a>
                                             </li>
                                             <!--<li>
                                                 <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                             </li>-->
                                             <li>
                                                 <a href="#" class="icon-info"><i class="fa fa-bell" aria-hidden="true"></i><span class="label label-primary">3</span></a>
                                             </li>
                                             <li>
                                               <a class="button" style="background-color: red" href="../login/logout.php" class="view btn-sm active"><i class="fa fa-sign-out" aria-hidden="true"></i><span class="hidden-xs">Logout</span></a>
                                             </li>
                                           <!--  <li class="dropdown">
                                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                     <img src="http://jskrishna.com/work/merkury/images/user-pic.jpg" alt="user">
                                                     <b class="caret"></b>
                                                 </a>
                                                 <ul class="dropdown-menu">
                                                     <li>
                                                         <div class="navbar-content">
                                                             <span><?php echo $user_check ?></span>
                                                             <p class="text-muted small">
                                                         me@jskrishna.com</p>
                                                             <div class="divider">
 </div>
                                                             <a href="logout.php" class="view btn-sm active">Logout</a>
                                                         </div>
                                                     </li>
                                                 </ul>
                                             </li> -->
                                         </ul>
                                     </div>
                                 </div>
                             </header>
                         </div>
                         <div id=dashboard class="user-dashboard">


                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal fade" role="dialog">
                 <div id="addEntry" class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                         <div class="modal-header login-header">
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                             <h4 class="modal-title"></h4>
                         </div>
                         <div class="modal-body">
                             <label for="">Ora di entrata</label>
                             <input id="ora" type="time" placeholder="hh:mm" name="orario_entrata">
                         </div>
                         <div class="modal-footer">
                             <button type="button" class="cancel" data-dismiss="modal">Annulla</button>
                             <button id="save" type="button" class="add-project" data-dismiss="modal" onclick="insert(this.id)">Salva</button>
                         </div>
                     </div>
                 </div>
             </div>
         </body>
         <script type="text/javascript">
     $(document).ready(function(){
        $('[data-toggle="offcanvas"]').click(function(){
            $("#navigation").toggleClass("hidden-xs");
        });
     });



     </script>
     </body>
 </html>
