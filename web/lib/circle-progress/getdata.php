<script src="../lib/jquery/jquery-3.2.0.min.js"></script>
<script src="../lib/jquery/dist/circle-progress.js"></script>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">

<script>
  $(document).ready(function() {
    circle();
  });
  function stampa(id){
    console.log(id);
  }
</script>
<?php
include ("../connessione.php");
$sql = "SELECT * FROM (SELECT COUNT(*) as totale, FK_classe as classe FROM alunno GROUP BY FK_classe) as x LEFT JOIN (SELECT COUNT(*) as presenti, FK_classe FROM registrazione JOIN alunno ON FK_alunno = alunno.ID_alunno WHERE datareg = CURDATE() and orario_uscita IS NULL GROUP BY FK_classe) as y ON x.classe = y.FK_classe";
$result= $connessione->query($sql);
if (!$result->num_rows>0){
  echo "No result";
}else{ ?>
  <ul class="nav nav-tabs" role="tablist">
  <?php while ($row=$result->fetch_assoc()) { ?>
    <li role="presentation" class="active"><div id="<?php echo $row['classe']?>" value="<?php echo ($row['presenti']/$row['totale'])?>" class="circle" onclick="stampa(this.id)"></div></li>
  <?php } ?>
  </ul>
<?php } ?>
<script>
function circle (){
  $('.circle').each(function(){
    $(this).circleProgress({
      value: $(this).attr("value"),
      size: 100,
      fill: {
        gradient: ["red", "orange"]
      }
    })
  });
}
</script>
