<?php
   session_start();

   if(session_destroy()) {
      setcookie("classe", "", time()-3600);
      header("Location: login.php");
   }
?>
