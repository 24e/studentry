<?php
include("connessione.php");
include("../telegram/bot.php");

$sql = "UPDATE registrazione SET orario_uscita = IF (orario_uscita IS NULL, NOW(), orario_uscita);";

if (!$connessione->query($sql)) {
    echo "Errore: ". $connessione->error . ".<br />";
}

$sql = "SELECT ID, valore FROM calendario WHERE giorno = CURDATE()";

if (!$result = $connessione->query($sql)) {
    echo "Errore: ". $connessione->error . ".<br />";
}else {
  $row = $result->fetch_assoc();
  if($row[valore]=='scolastico'){
    $FK_calendario = $row[ID];
    //echo $FK_calendario."<br><br>";
    $sql = "SELECT alunno.ID_alunno FROM (SELECT * FROM registrazione WHERE datareg = CURDATE()) as tmp RIGHT JOIN alunno ON tmp.FK_alunno = alunno.ID_alunno WHERE tmp.ID IS NULL";

    if (!$result = $connessione->query($sql)) {
        echo "Errore: ". $connessione->error . ".<br />";
    }else {
      while($row = $result->fetch_assoc()){
        $alunno = $row[ID_alunno];
        $sql = "INSERT INTO assenza (FK_alunno, FK_calendario) VALUES (".$alunno.",".$FK_calendario.")";
        $connessione->query($sql);
        $sql = "SELECT nome, chat_id FROM alunno WHERE ID_alunno = ".$alunno;
        //echo $sql;
        $connessione->query($sql);
        if($result = $connessione->query($sql)){
          if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            $messaggio = "L'alunno ".$row[nome]." oggi e' stato assente alle lezioni scolastiche";
            inviaMessaggio($row[chat_id], $messaggio);
          }
        }
      }
    }
  }
}

?>
