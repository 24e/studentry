<script type="text/javascript">
$(".presente").click(function(){
    $('.entrata').slideUp("slow");
    $('.add').show();
    $(this).parent().parent().next().slideDown("slow");
    $(this).hide();
});

$(".inserisci").click(function(){
  $(this).parent().parent().next().slideUp("slow");
  $(this).parent().fadeOut("slow");
  $.post( "../script/inserisci.php", {
    id: $(this).attr('id'),
    hour: $(this).prev().prev().prop('value'),
    min: $(this).prev().prop('value')
  }).done(function(){
          load();
  });
});
</script>

<?php
include("connessione.php");
//$classe = $_GET['classe'];
$sql_code =   "SELECT * FROM
                (SELECT * FROM registrazione
                  WHERE datareg = CURDATE()) tmp RIGHT JOIN alunno
                ON alunno.ID_alunno = tmp.FK_alunno
                WHERE (orario_uscita IS NOT NULL or orario_entrata IS NULL) and alunno.FK_classe =".$_COOKIE['classe'] ." ORDER BY alunno.ID_alunno"; //and alunno.FK_classe = 2*/
//$sql_code = "SELECT * FROM registrazione RIGHT JOIN alunno ON registrazione.FK_alunno = alunno.ID_alunno WHERE (datareg = CURDATE() or datareg IS NULL) and alunno.ID_alunno NOT IN (SELECT FK_alunno FROM registrazione WHERE orario_uscita IS NULL) and FK_classe = ".$classe;

$result = $connessione->query($sql_code);
$connessione->close();
//$s = '';
if ($result->num_rows > 0) {
  while ($rowreg = $result->fetch_assoc()) {
    //if($rowreg['orario_uscita']==null)  ?>
    <div id="<?php echo $rowreg['ID_alunno'] ?>" class="info assenti">
      <h2><?php echo $rowreg['nome'] ?></h2>
      <div class="btn-group">
        <?php if ($rowreg['orario_uscita'] > 0) { ?>
          <button disabled class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span>Uscita: </span><?php echo $rowreg['orario_uscita'] ?>
          </button>
        <?php } ?>
        <button id="<?php echo $rowreg['ID_alunno'] ?>" value="<?php echo $rowreg['nome'] ?> " class="button add presente" data-toggle="modal" hidden="true"><span>Presente</span></button>
      </div>
    </div>
    <div id="<?php echo $rowreg['ID_alunno'] ?>" class="entrata">
      <span>Ora di entrata</span><br><input type="number" name="" value="" min="8" max="17" placeholder="HH" style="width: 70px"> : <input type="number" name="" value="" min="0" max="59" placeholder="MM" style="width: 70px">
      <button id="<?php echo $rowreg['ID_alunno'] ?>" value="<?php echo $rowreg['nome'] ?> " class="inserisci button presente" data-toggle="modal" onclick="" hidden="true"><span>  Inserisci  </span></button>
    </div>
  <?php }
} else { ?>
    <div id="noresult" class="info" draggable="false">
      <h2>No result</h2>
    </div>
  <?php } ?>
