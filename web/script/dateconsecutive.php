<?php
include("connessione.php");
setlocale(LC_TIME, 'ita');
//$classe = $_COOKIE['classe'];//$_POST['classe'];
//and orario_entrata > ".$time."
$sql_code  =  "SELECT alunno.nome, assenza.ID, calendario.giorno FROM assenza JOIN alunno ON assenza.FK_alunno = alunno.ID_alunno JOIN calendario ON assenza.FK_calendario = calendario.ID WHERE alunno.ID_alunno = ".$_POST[id]." ORDER BY calendario.giorno";

$result = $connessione->query($sql_code);
if($result->num_rows>0){

  $dates = array();
  while ($row = $result->fetch_assoc()) {
    $dates[] = new DateTime($row['giorno']);
  }

  // process the array

  $lastDate = null;
  $ranges = array();
  $currentRange = array();

  foreach ($dates as $date) {

      if (null === $lastDate) {
          $currentRange[] = $date;
      } else {

          // get the DateInterval object
          $interval = $date->diff($lastDate);

          // DateInterval has properties for
          // days, weeks. months etc. You should
          // implement some more robust conditions here to
          // make sure all you're not getting false matches
          // for diffs like a month and a day, a year and
          // a day and so on...

          if ($interval->days === 1) {
              // add this date to the current range
              $currentRange[] = $date;
          } else {
              // store the old range and start anew
              $ranges[] = $currentRange;
              $currentRange = array($date);
          }
      }

      // end of iteration...
      // this date is now the last date
      $lastDate = $date;
  }

  // messy...
  $ranges[] = $currentRange;

  // print dates
  echo "<ul>";
  foreach ($ranges as $range) {

      // there'll always be one array element, so
      // shift that off and create a string from the date object
      $startDate = array_shift($range);
      $str = sprintf('%s', $startDate->format('D j M'));

      // if there are still elements in $range
      // then this is a range. pop off the last
      // element, do the same as above and concatenate
      if (count($range)) {
          $endDate = array_pop($range);
          $str .= sprintf(' to %s', $endDate->format('D j M'));
      }

      echo "<li><b>$str</b></li>";
  }
  echo "</ul>";


}

?>
