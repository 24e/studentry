<script type="text/javascript">
$('.giustifica').click(function(){
  //alert($(this).attr('id'));
  $(this).parent().parent().fadeOut('slow', function(event){
    $.post( "../script/giustifica.php", { id: $(this).attr('id') }, function(){
      //alert("Giustificata");
    })
    .done(function(){
      //alert('reload');
      //getdata(Cookie.get("classe"));
      $('#ritardi').load('../script/ritardi.php');
      $('#assenze').load('../script/datigiustifiche.php');

    })
  });
});
</script>
<?php
include("connessione.php");
if (!$_COOKIE['classe']>0) { ?>
  <div id="noresult" class="info">
    <h2>No result</h2>
  </div>
<?php
}else {
  $classe = "and alunno.FK_classe=".$_COOKIE['classe'];
  $sql = "SELECT alunno.nome, assenza.ID, calendario.giorno FROM assenza JOIN alunno ON assenza.FK_alunno = alunno.ID_alunno JOIN calendario ON assenza.FK_calendario = calendario.ID WHERE giustificata IS NULL ".$classe ;
  if (!$result = $connessione->query($sql)) {
      echo "Errore: ". $connessione->error . ".<br />";
  }else {
    if($result->num_rows > 0){
      while($row = $result->fetch_assoc()){
        $giorno = new DateTime($row['giorno']);
        ?>
          <div id="<?php echo $row['ID']; ?>" class="info giustificazioni" value="">
            <h2><?php echo $row['nome'];?></h2>
            <div class="btn-group">
              <button disabled id="<?php echo $row['ID']?>" onclick="" class="btn btn-secondary btn-lg dropdown-toggle button" data-toggle="modal"><span>Il: </span><?php echo $giorno->format('d/m/Y'); ?></button>
              <button id="<?php echo $row['ID']; ?>" class="button giustifica" type="button" name="giustifica">Giustifica</button>
            </div>
          </div>
      <?php }
    }else{?>
      <div id="noresult" class="info">
        <h2>No result</h2>
      </div>
  <?php
    }
  }
}
?>
