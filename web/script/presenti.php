<script type="text/javascript">
//approvazione entrata (in ritardo) e aggiornamento dati div presenti
function approve(id){
  //richiesta UPDATE
  $.post("../script/approve.php", {q: id}).done(function(){
    //aggiornamento div presenti
    $('#presenti').load('../script/presenti.php');
  });
}

$(".assente").click(function(){
  $(this).parent().parent().parent().fadeOut("slow");
  $.post( "../script/rimuovi.php", {
    id: $(this).parent().parent().parent().attr('value')
  }).done(function(){
          load();
  });
});
</script>

<?php

include("connessione.php");
//$classe = $_COOKIE['classe'];//$_POST['classe'];
//and orario_entrata > ".$time."
$sql_code  =  "SELECT * FROM registrazione
              JOIN alunno ON alunno.ID_alunno = registrazione.FK_alunno
              WHERE datareg = CURDATE()

              and orario_uscita IS NULL
              and alunno.FK_classe = ".$_COOKIE['classe'];/*and approve is true
              ORDER BY alunno.ID_alunno ASC ";*/

$result = $connessione->query($sql_code);


if ($result->num_rows > 0) {
    //$i = 0;
    while ($rowreg = $result->fetch_assoc()) {
      ?>
      <div id="<?php echo $rowreg['ID_alunno']; ?>" class="info presenti" value=" <?php echo $rowreg['ID']?>">
        <h2><?php echo $rowreg['nome'] ?></h2>
        <div>
          <?php
            if (!$rowreg['approve']) { ?>
            <div class="btn-group">
              <button id="<?php echo $rowreg['ID']?>" onclick="approve(this.id)" class="button approve" data-toggle="modal" >Approva</button>
            </div>
          <?php
            } ?>
          <div class="btn-group">
            <button disabled class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="hidden-xs">Entrata: </span><span class="visible-xs">E: </span><?php echo $rowreg['orario_entrata']?>
            </button>
            <button id="<?php echo $rowreg['ID_alunno'] ?>" value="<?php echo $rowreg['nome'] ?> " class="button add assente" data-toggle="modal" hidden="true"><span>Assente</span></button>
          </div>
        </div>
      </div>
    <?php
    } ?>
<?php
} else {
    ?>
  <div id="noresult" class="info">
    <h2>No result</h2>
  </div>
<?php

}
  $connessione->close();
//exit();
?>
