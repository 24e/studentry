<?php
include("../config/config.php");

//istanza dell'oggetto della classe mysqli
$connessione = new mysqli($host, $user, $password, $db);
//verifica su eventuali errori di connessione
if ($connessione->connect_errno) {
    echo "Connessione fallita: ". $connessione->connect_error .".";
    exit();
}

?>
