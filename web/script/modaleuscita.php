<script type="text/javascript">
$(document).ready(function(){
  $('.save').click(function() {
      alert($("#hh125").val())
    });
});


</script>

<?php
include("connessione.php");

$sql = "SELECT nome FROM classe WHERE ID_classe = ".$_COOKIE['classe'];

$result = $connessione->query($sql);

//echo $result->num_rows;
if ($result->num_rows > 0) {
    //$i = 0;
    while ($row = $result->fetch_assoc()) {
        $nome_classe = $row['nome'];
    }
  }
?>

<div class="modal-content">
    <div class="modal-header login-header">
      <button type="button" class="close cancel" >X</button>
      <h3 class="modal-title">Classe: <?php echo $nome_classe; ?></h3>
    </div>

    <div id="bodymodale" class="modal-body">
      <form class="form-horizontal" action="" method="post">
        <div class="form-group form-group-lg">
          <label class="col-sm-4 control-label">Alunno:</label>
          <div class="input-group col-sm-6">
            <select id="alunno" class="form-control" name="alunno">
              <option disabled value="">Seleziona alunno</option>
              <?php
                $sql = "SELECT registrazione.ID, alunno.nome FROM registrazione
                              JOIN alunno ON alunno.ID_alunno = registrazione.FK_alunno
                              WHERE datareg = CURDATE()

                              and orario_uscita IS NULL
                              and alunno.FK_classe = ".$_COOKIE['classe'];

                $result = $connessione->query($sql);
                if($result->num_rows > 0){
                  while ($row = $result->fetch_assoc()) {?>
                    <option value="<?php echo $row['ID'];?>" name="<?php echo $row['nome'];?>"><?php echo $row['nome'];?></option>
                    <?php
                  }
                }else {?>
                  <option disabled value="">Nessun risultato</option>
                  <?php
                }
              ?>
            </select>
          </div>
        </div>
        <div class="form-group form-group-lg">
          <label class="col-sm-4 control-label">Ora di uscita:</label>
          <div id="orario" class="input-group col-sm-6">
              <input required class="form-control" id="hh125" type="number" placeholder="HH" name="hh125">
              <div class="input-group-addon">:</div>
              <input required class="form-control" id="mm" type="number" placeholder="MM" name="mm">
          </div>
          <label class="col-sm-4 control-label">Permesso telefonico</label>
          <div class="input-group col-sm-6">
            <input class="form-control" id="permesso" type="checkbox" name="permesso" value="permesso">
          </div>
        </div>



      </form>
    </div>

    <div id="footermodale" class="modal-footer">
        <button id="exit_button" type="button" class="cancel" >Annulla</button>
        <button id="save" type="button" class="add-project save" >Salva</button>
    </div>
</div>
