<script type="text/javascript">
  //approvazione entrata (in ritardo) e aggiornamento dati div ritardi
  function approve(id){
    //richiesta UPDATE
    $.post("../script/approve.php", {q: id}).done(function(){
      //aggiornamento div presenti
      $('#ritardi').load('../script/ritardi.php');
    });
  }
</script>
<?php
include ("connessione.php");
$sql_code = "SELECT * FROM registrazione
              JOIN alunno ON FK_alunno = ID_alunno
              WHERE approve = 0
              and datareg = CURDATE()
              and alunno.FK_classe = ".$_COOKIE['classe'];
$result = $connessione->query($sql_code);
if ($result->num_rows > 0) {
  while ($rowreg = $result->fetch_assoc()) { ?>
    <div id="<?php echo $rowreg['ID_alunno']; ?>" class="info presenti" value=" <?php echo $rowreg['ID']?>">
      <h2><?php echo $rowreg['nome'] ?></h2>
      <div>
        <div class="btn-group">
          <button id="<?php echo $rowreg['ID']?>" onclick="approve(this.id)" class="button" data-toggle="modal" >Approva</button>
        </div>
        <div class="btn-group">
          <button disabled class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="hidden-xs">Entrata: </span><span class="visible-xs">E: </span><?php echo $rowreg['orario_entrata']?>
          </button>
        </div>
      </div>
    </div>
  <?php
      //echo $row['nome']." ".$row['orario_entrata'].' <button type="button" value="'.$row['ID_alunno'].'" onclick="approve(this.value)">Approve</button>';
  }
}else {?>
  <div id="noresult" class="info">
    <h2>No result</h2>
  </div>
<?php
}
?>
