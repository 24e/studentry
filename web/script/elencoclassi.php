<script src="../lib/jquery/jquery-3.2.0.min.js"></script>
<script src="../lib/jquery/dist/circle-progress.js"></script>

<script>
  $(document).ready(function() {
    //circle();
  });
</script>
<?php
session_start();
include ("connessione.php");
//include("../login/session.php"); //se è un segretario devo mostrare tutte le classi perchè nel db le classi sono assegnate solo ai docenti (giustamente)
if ($_SESSION['tipo']=="docente") {
  $sql = "SELECT * FROM (SELECT COUNT(*) as totale, classe.ID_classe as classe, classe.nome as nomeclasse FROM alunno JOIN classe ON alunno.FK_classe = classe.ID_classe WHERE classe.ID_classe IN (SELECT FK_classe FROM lezione WHERE FK_docente = ".$_SESSION['userID'].") GROUP BY classe.ID_classe) as x LEFT JOIN (SELECT COUNT(*) as presenti, FK_classe FROM registrazione JOIN alunno ON FK_alunno = alunno.ID_alunno WHERE datareg = CURDATE() and orario_uscita IS NULL GROUP BY FK_classe) as y ON x.classe = y.FK_classe ";
  //alert('ciao');
} else {
  $sql = "SELECT * FROM (SELECT COUNT(*) as totale, classe.ID_classe as classe, classe.nome as nomeclasse FROM alunno JOIN classe ON alunno.FK_classe = classe.ID_classe GROUP BY classe.ID_classe) as x LEFT JOIN (SELECT COUNT(*) as presenti, FK_classe FROM registrazione JOIN alunno ON FK_alunno = alunno.ID_alunno WHERE datareg = CURDATE() and orario_uscita IS NULL GROUP BY FK_classe) as y ON x.classe = y.FK_classe ";
}
$result= $connessione->query($sql);
if (!$result->num_rows>0){
  echo $sql;
  echo "No result";
}else{ ?>
  <!--<div class="tab row placeholders">-->
  <?php while ($row=$result->fetch_assoc()) {
    if ($row['classe']==$_COOKIE['classe']) {?>
      <div id="classe<?php echo $row['classe']?>" value="<?php echo $row['classe']?>" class="col-xs-6 col-sm-2 placeholder tablinks active" >
    <?php }else{ ?>
      <div id="classe<?php echo $row['classe']?>" value="<?php echo $row['classe']?>" class="col-xs-6 col-sm-2 placeholder tablinks" >
    <?php }?>
    <div id="<?php echo $row['classe']?>" value="<?php echo ($row['presenti']/$row['totale'])?>" class="circle" >
      <div class="value">

      </div>
    </div>
    <h4><?php echo $row['nomeclasse']?></h4>
    <span class="text-muted"><?php echo "Assenti: ".($row['totale']-$row['presenti'])."/".$row['totale']?></span>
  </div>
  <?php } ?>

<?php } ?>
<script>
function circle (){

  $('.circle').each(function(){
    $(this).circleProgress({
      value: $(this).attr("value"),
      size: 100,
      fill: {
        gradient: ["blue", "green", "red", "yellow"]
      }
    }).on('circle-animation-progress', function (e, p, v) {
      $(this).children('.value').text((v * 100 ).toFixed()+"%");
    });
  });
}
</script>
