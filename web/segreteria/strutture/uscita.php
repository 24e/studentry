<script type="text/javascript">
function load(){
  $('#nav_classi').load('../script/elencoclassi.php', function(){
      $('.circle').each(function(){
        $(this).circleProgress({
          value: $(this).attr("value"),
          size: 100,
          fill: {
            gradient: ["blue", "green", "red", "yellow"]
          }
        }).on('circle-animation-progress', function (e, p, v) {
          $(this).children('.value').text((v * 100 ).toFixed()+"%");
        });
      });

      $('.tablinks').click(function(){
        Cookies.set("classe", $(this).attr('value'));
        $('.tablinks').removeClass("active");
        $(this).addClass("active");
        $('#ritardi').load('../script/ritardi.php');
        //$('#assenti').load('../script/assenti.php');
        $('#btn_fatto').hide();
        $('#btn_modifica').show();
      });


    });
  $('#ritardi').load('../script/ritardi.php');

  $('#btn_fatto').hide();
  $('#btn_modifica').show();
  }
</script>

<div class="" style="text-align: center">
  <h3><strong>Elenco classi</strong></h3>
</div>
<div class="tab row placeholders">

  <div id="nav_classi">

  </div>

</div>
  <div class="row">
    <div class="col-md-2 col-sm-2"></div>
      <div class="col-md-8 col-sm-8 col-xs-12 gutter">
          <h1 style="font-weight: bold">Ritardi</h1>
          <div id="ritardi" >

          </div>
      </div>
  </div>
  <div id="datainfo" class="tabcontent">

  </div>
