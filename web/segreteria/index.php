<!-- JQUERY LOADER 2 -->
<script src="../lib/queryloader2/queryloader2.min.js" type="text/javascript"></script>


<?php
//include("../login/session.php");
//if(!isset($_SESSION['login_user'])){
  // header("location:../login/login.php");
//}else {
  include("strutture/index.html");
//}
?>

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', function() {
        QueryLoader2(document.querySelector("body"), {
            barColor: "#efefef",
            backgroundColor: "#111",
            percentage: true,
            barHeight: 1,
            minimumTime: 200,
            fadeOutTime: 1000
        });
    });
</script>

<!DOCTYPE html>
<html lang="it">

<head>
    <meta charset="utf-8">
    <title>Segreteria</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- JQUERY -->
    <script src="../lib/jquery/jquery-3.2.0.min.js"></script>
    <!-- JQUERY COOKIE -->
    <script src="../lib/js-cookie-master/src/js.cookie.js"></script>
    <!-- TEMA PRINCIPALE -->
    <link href="../lib/bootstrap/css/my.css" rel="stylesheet">
    <!-- TEMA BASE BOOTSTRAP -->
    <link href="../lib/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
    <!--FAFA ICON -->
    <link rel="stylesheet" href="../lib/font-awesome-4.7.0/css/font-awesome.min.css">


    <script>
        $(document).ready(function() {
            $('#dashboard').load('strutture/home.html');
        });
    </script>
    <script type="text/javascript">
        //caricamento della user-dashboard
        function dashload(id) {
            var classe = Cookies.get("classe");
            //classe active al div classe cliccato
            $('#nav').children().children().each(function() {
                if ($(this).attr('id') == id) {
                    $(this).attr('class', 'active');
                } else {
                    $(this).attr('class', '');
                }
            });

            //caricamento pagina selezionata dashboard
            $('#dashboard').load($('#' + id).children().attr('href'));
        }

        //approvazione entrata (in ritardo) e aggiornamento dati div presenti
        function approve(id) {
            //richiesta UPDATE
            $.post("../script/approve.php", {
                q: id
            }).done(function() {
                //aggiornamento div presenti
                $('#ritardi').load('../script/ritardi.php');
            });
        }


        $(function() {
            $('.modal').hide();
            $('#btn_uscita').click(function(e) {
                $(this).hide();
                $("#btn_fatto").show();
                console.log($(this).attr('href'));
                e.preventDefault();

                $('#modal1').load('../script/modaleuscita.php', function() {
                    $('<div class="overlay"></div>').appendTo('body').fadeIn();
                    $('#modal1').clone().attr('id', 'modal').appendTo('.overlay').show();

                    $('.save').click(function() {
                        alert("Inserire controlli su presenza dati!! (segreteria/index.php:101)");
                        //alert();
                        $.post( "../script/permesso.php", {
                          id: $('#modal').find('select').val(),
                          ora: $('#modal').find('input[name="hh125"]').val(),
                          min: $('#modal').find('input[name="mm"]').val(),
                          permesso: $('#modal').find('input[name="permesso"]').prop("checked")
                        }).done(function(){
                          $('#modal').find('#bodymodale').html('<p>Registrazione permesso di uscita per l\'alunno '+$('#alunno').attr("name")+' alle ore '+$('#modal').find('input[name="hh125"]').val()+':'+$('#modal').find('input[name="mm"]').val()+' avvenuta con successo!</p>');
                          $('#modal').find('#footermodale').html('<button id="modalconfirm" class="cancel" type="button">Conferma</button>');
                          $('#modalconfirm').click(function() {
                              $('.overlay').remove();
                              $("#btn_fatto").hide();
                              $("#btn_uscita").show();
                          });
                        });
                    });

                    $('.cancel').click(function() {
                        $('.overlay').remove();
                        $("#btn_fatto").hide();
                        $("#btn_uscita").show();
                    });

                });
            });
        });


        //-----------------------------------------------------------------------------------//
        //-----------------------------------------------------------------------------------//
        //                                                                                   //
        //       IN TEORIA LE PROSSIME FUNZIONI NON VENGONO UTILIZZATE - [controllare]       //
        //                                                                                   //
        //-----------------------------------------------------------------------------------//
        //-----------------------------------------------------------------------------------//
        function insert(id) {
            var time = $("#ora").attr("value");
            $.ajax({
                type: "POST",
                url: "../index01l.php",
                data: {
                    id: id,
                    time: $("#ora").attr("value")
                }
            });
        }





        function dialog(nome, id) {
            $(function() {
                $("#addEntry").dialog({
                    modal: true
                }).find('h4').html(nome).attr("id", id);
                $("#save").attr({
                    "id": id
                });
                //$('.modal-footer').children().next().next(),attr({"id": id});
            });
        }

        function drag() {
            //if ($('#presenti0').draggable("disable")) {
            $('.presente').each(function() {
                $(this).draggable({
                    disabled: false,
                    helper: 'clone',
                    opacity: 0.8
                });
            });
            /*.draggable(/*{
                    helper: 'clone',
                    opacity: 0.5
                  }*/
            /*}else {
              $('#presenti0').draggable("disable");
            }*/
            $("#assenti").droppable({
                accept: ".presente",
                drop: function(event, ui) {
                    //$('.sales#0').find('div').html('Eccomi!');
                    $(ui.draggable).html('<div class="btn-group"><button onclick="#add_project" class="add-project presente" data-toggle="modal">Presente</button></div>').attr({
                            "class": presenti
                        }
                        /* {
                                 constructor() {

                                 }
                               }*/
                    );
                    $(ui.draggable).appendTo('#assenti');
                    //$('.sales#0').insertAfter('#assenti');

                    /*  var new_signature = $(ui.helper).clone();/*.removeClass('sales');*/
                    /*new_signature.draggable();
                    $(this).append(new_signature);*/
                }
            });



        }
    </script>

</head>


<body class="home">


    <div class="container-fluid display-table">
        <div class="row display-table-row">
            <div class="col-md-2 col-sm-1 hidden-xs display-table-cell v-align box" id="navigation">
                <div class="logo">
                    <a hef="home.html">
                        <img src="../images/logo.png" alt="merkery_logo" class="img-responsive">
                        <!--<img src="http://jskrishna.com/work/merkury/images/circle-logo.png" alt="merkery_logo" class="visible-xs visible-sm circle-logo">-->
                    </a>
                </div>
                <div id="nav" class="navi">
                    <ul>
                        <li id="h" class="active" onclick="dashload(this.id)">
                            <a href="strutture/home.html" onclick="return false"><i class="fa fa-home" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Home</span></a>
                        </li>
                        <!--<li id="g" onclick="dashload(this.id)">
                            <a href="#" onclick="return false"><i class="fa fa-tasks" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Giustificazioni</span></a>
                        </li>
                        <li id="s" onclick="dashload(this.id)">
                            <a href="#" onclick="return false"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Storico Assenze</span></a>
                        </li>-->
                        <li id="p" onclick="dashload(this.id)">
                            <a href="#" onclick="return false"><i class="fa fa-user" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Profilo</span></a>
                        </li>
                        <li id="o" onclick="dashload(this.id)">
                            <a href="strutture/badge.php" onclick="return false"><i class="fa fa-id-badge" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Assegna Badges</span></a>
                        </li>
                        <li id="i" onclick="dashload(this.id)">
                            <a href="#" onclick="return false"><i class="fa fa-cog" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Impostazioni</span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-10 col-sm-11 display-table-cell v-align">
                <!--<button type="button" class="slide-toggle">Slide Toggle</button> -->
                <div class="row">
                    <header>
                        <div class="col-md-7">
                            <nav class="navbar-default pull-left">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#side-menu" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                </div>
                            </nav>
                            <div class="search">
                                <input type="text" placeholder="Search" id="search">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="header-rightside">
                                <ul class="list-inline header-top pull-right">
                                    <!--<li class="hidden-xs"><a href="#" onclick="drag()" class="add-project" data-toggle="modal"><i class="fa fa-pencil" aria-hidden="true"></i>  Modifica</a></li>-->
                                    <li>
                                        <a id="btn_uscita" href="#modal1" class="button mod"><i class="fa fa-plus-square" aria-hidden="true"></i><span class="hidden-xs">Uscita</span></a>
                                        <a id="btn_fatto" href="#" onclick="change(this.id)" class="button mod" data-toggle="modal"><i class="fa fa-check-circle" aria-hidden="true"></i>Fatto</a>
                                    </li>
                                    <!--<li>
                                        <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                    </li>-->
                                    <li>
                                        <a href="#" class="icon-info"><i class="fa fa-bell" aria-hidden="true"></i><span class="label label-primary">3</span></a>
                                    </li>
                                    <li>
                                        <a class="button" style="background-color: red" href="../login/logout.php" class="view btn-sm active"><i class="fa fa-sign-out" aria-hidden="true"></i><span class="hidden-xs">Logout</span></a>
                                    </li>
                                    <!--  <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <img src="http://jskrishna.com/work/merkury/images/user-pic.jpg" alt="user">
                                                    <b class="caret"></b>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <div class="navbar-content">
                                                            <span><?php echo $user_check ?></span>
                                                            <p class="text-muted small">
                                                        me@jskrishna.com</p>
                                                            <div class="divider">
</div>
                                                            <a href="logout.php" class="view btn-sm active">Logout</a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li> -->
                                </ul>
                            </div>
                        </div>
                    </header>
                </div>
                <div id=dashboard class="user-dashboard">


                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog">
        <div id="modal1" class=" modal modal-dialog">
            <!-- Modal content-->

        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="offcanvas"]').click(function() {
            $("#navigation").toggleClass("hidden-xs");
        });
    });
</script>
</body>

</html>
