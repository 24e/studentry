####[DA TOGLIERE] Connessione del WebClient

Per connettersi alla rete si utilizzano le librerie `Ethernet.h` e `SPI.h`. (?)
L'Arduino si connette alla rete come un normalissimo client, avrà quindi bisogno di un indirizzo MAC. Nelle Shield Ethernet più recenti questo è indicato nel retro della scheda, nel caso in cui questo non sia presente è possibile sceglierne uno a piacere, che non deve però essere in confilitto con quelli degli altri dispositivi connessi. Per configurare il MAC Address bisogna impostare come componenti del vettore `byte mac[]` le 6 coppie di valori esadecimali (1 Byte ognuna) con la sintassi `0x<coppia_esadecimale>`, come riportato nel codice sottostante.
Per connettersi servirà inoltre un indirizzo IPv4. La scheda proverà prima a connettersi alla rete ottenendo l'indirizzo dal DHCP Server, se questa configurazione fallisce proverà a connettersi con l'indirizzo specificato nella variabile `IPAddress ip`. Nel caso in cui la configurazione dell'IP avvenga tramite DHCP bisogna ricordarsi che quest'indirizzo sarà disponibile per il *tempo di lease* configurato dall'amministratore di rete e che dovrà essere quindi rinnovato prima della sua scadenza oppure riconfigurato, operazioni che possono fallire. Viene perciò implementata la funzione `void maintain()` al fine di gestire questa configurazione nelle sue 4 casistiche.
Di seguito riportato il codice necessario a connettersi.
```cpp
#include <SPI.h>
#include <Ethernet.h>

byte mac[] = {
	0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 // mac address dell'Ethernet Shield
};
IPAddress ip(192, 168, 0, 128); //indirizzo IP Arduino, se fallisce il DHCP
EthernetClient client; // istanzio la libreria EthernetClient
    
void setup() {
	...
    
    // connessione attraverso DCHP 


    if (Ethernet.begin(mac) == 0) {
     	Serial.println("Connessione con DHCP fallita");
    	// connessione con indirizzo IP statico, se la configurazione con DHCP fallisce
        Ethernet.begin(mac, ip);
    }
        
    // si da all' Ethernet shield un secondo per inizializzarsi
    delay(1000);
    Serial.println("connecting...");
    printIPAddress();
  	
    ...
}
    
void loop() {
   	// viene richiamata la funzione per il mantenimento della connessione con DHCP
    maintain();
    ...
}
      
void maintain(){
	switch (Ethernet.maintain()){
    	case 1:
        	//renewed fallito
            Serial.println("Error: renewed fail");
            break;
            
        case 2:
        	//renewed avvenuto con successo
            Serial.println("Renewed success");
            //stampa l'indirizzo IP:
            printIPAddress();
            break;
            
        case 3:
        	//rebind fallito
            Serial.println("Error: rebind fail");
            break;
            
        case 4:
        	//rebind avvenuto con successo
            Serial.println("Rebind success");
            //stampa l'indirizzo IP:
            printIPAddress();
            break;
            
    	default:
        	//non succede nulla
            break;
	}       
}
    
void printIPAddress(){
	Serial.print("My IP address: ");
  	for (byte thisByte = 0; thisByte < 4; thisByte++) {
       	// stampa il valore di ogni byte dell'indirizzo IP
    	Serial.print(Ethernet.localIP()[thisByte], DEC);
    	Serial.print(".");
  	}
	Serial.println();
}
```
####[DA TOGLIERE] Lettura Card RFiDParti_tolte


##

Per quanto riguarda il DDL (Data Definition Language) del database di seguito sono riportate alcune parti di codice SQL:

Creazione tabella alunno: 

    CREATE TABLE alunno (
      ID_alunno INT(11) NOT NULL,
      nome VARCHAR(20) DEFAULT NULL,
      numerotessera VARCHAR(12) DEFAULT NULL,
      chat_id VARCHAR(10),
      telegram_username(50),
      FK_classe INT(11) NOT NULL
    )

Creazione tabella classe 

    CREATE TABLE `classe` (
      `ID_classe` INT(11) NOT NULL PRIMARY KEY,
      `nome` VARCHAR(4) DEFAULT NULL
    ) 

Per quanto riguarda il DML (Data Manipulation Language) del database, essendo che le relazione fra le entità del database sono tutte di tipo UNO A MOLTI, di seguito è riportata la creazione della relazione più significativa ovvero quella tra alunno e relativa classe di appartenenza:

    ALTER TABLE `alunno`
      ADD PRIMARY KEY (`ID_alunno`),
      ADD UNIQUE KEY `numerotessera` (`numerotessera`),
      ADD CONSTRAINT `frequente` FOREIGN KEY (`FK_classe`) 
      REFERENCES `classe` (`ID_classe`);

Inserimento valori alunni:

    INSERT INTO `alunno` (`ID_alunno`, `nome`, `numerotessera`, `telegram_username`, `chat_id`, `FK_classe`) VALUES
    (1, 'alunno01', '111', 'n_possamai', '19521412', 1),
    (2, 'alunno02', '222', 'e_baldovin', '19521412', 2),
    (3, 'alunno03', '333', 't_lyacoubi', '386548732', 3),

Inserimento valori classi:

	INSERT INTO `classe` (`ID_classe`, `nome`) VALUES                       
    (1, '5AIA'),
    (2, '5AEC'),
    (3, '5AAM'),
    (4, '5ACA'),
    (5, '5AET');