
# **StudEntry - Gestionale presenze**


> Sviluppato da: Erik Baldovin, Tarik Lyacoubi, Nicholas Possamai


## **Indice**
<!-- TOC depthFrom:2 depthTo:4 withLinks:1 updateOnSave:1 orderedList:0 -->

- [**Indice**](#indice)
- [**Introduzione**](#introduzione)
- [**Analisi**](#analisi)
- [**Materiali e risorse utilizzate**](#materiali-e-risorse-utilizzate)
	- [**Hardware**](#hardware)
	- [**Software (librerie esterne)**](#software-librerie-esterne)
	- [**Linguaggi**](#linguaggi)
	- [**Coordinamento del gruppo**](#coordinamento-del-gruppo)
	- [**Studio e risoluzione dei problemi**](#studio-e-risoluzione-dei-problemi)
- [**Database**](#database)
- [**Arduino**](#arduino)
	- [**Scatch timbratura alunni**](#scatch-timbratura-alunni)
	- [**Scatch assegnazione badge**](#scatch-assegnazione-badge)
- [**Applicativo Web**](#applicativo-web)
- [**App Android**](#app-android)
	- [**Problematiche iniziali**](#problematiche-iniziali)
	- [**Concezione e Progettazione**](#concezione-e-progettazione)
	- [**Realizzazione**](#realizzazione)
	- [**Funzionalità e funzionamento dell'applicazione**](#funzionalit-e-funzionamento-dellapplicazione)
- [**Bot Telegram**](#bot-telegram)
- [**Sviluppi futuri**](#sviluppi-futuri)
- [**Conclusioni finali**](#conclusioni)

<!-- /TOC -->
<div class="page-break" />

## **Introduzione**

StudEntry nasce nel gennaio 2017 dalla necessità di informatizzare la gestione delle presenze degli alunni di un istituto scolastico.
L'idea del progetto, fornita dal Prof. Marco Massenz, è stata elaborata prendendo come istituto di riferimento l'IIS "Segato-Brustolon" e successivamente adeguata per poter utilizzare StudEntry in qualsiasi scuola.
L'applicativo rileva le timbrature dei badge associati agli studenti attraverso un lettore RFiD e si compone di un'interfaccia Web e un'app Android, riservate ai docenti, in cui è possibile visualizzare e gestire la situazione delle proprie classi in tempo reale e un'altra interfaccia Web, riservata alla segreteria, in cui è possibile amministrare i dati per un corretto funzionamento.
Inizialmente era stato pensato di creare un'integrazione tra StudEntry ed il sistema di registro elettronico di ARGO Software, già adottato dalla scuola, che per essere implementata avrebbe richiesto un accesso alla struttura tecnica del portale. La fattibilità di questa funzione è stata valutata e discussa anche con il docente Massenz.
Essendo ARGO un software proprietario abbiamo richiesto al proprietario stesso informazioni per l’accesso alla struttura. Non avendo (ancora oggi) ricevuto alcuna risposta questa ipotesi è stata scartata, ma si tiene comunque in considerazione, per uno sviluppo futuro, la possibilità di importare i dati del registro in formato CSV.     
StudEntry prevede inoltre che i genitori degli studenti vengano notificati nel caso il loro figlio resti assente alle lezioni oppure effettui entrate posticipate e/o uscite anticipate.

![princ_funz](https://s10.postimg.org/nbqg2ge3t/Stud_Entry.pptx.png)

<div class="page-break" />

## **Analisi**

Durante le prime settimane di lavoro il gruppo si è incontrato molteplici volte per discutere il progetto suddividendolo in fasi e attività, relazionandosi alle proprie possibilità e risorse, iniziando a raccogliere le idee e raggruppare le varie opinioni e i differenti pensieri che ogni membro del team aveva a riguardo.
Successivamente sono state trascritte le nozioni principali riguardanti le diverse attività suddivise in sottoattività ed è stato scelto il materiale hardware e software da utilizzare per la loro realizzazione, valutando costi e conoscenze da impiegarvi.
Dunque prima dell'effettivo inizio dello sviluppo dell'applicativo sono state individuate le funzionalità da integrarvi, raggruppate come di seguito:

**Indispensabili:**

- Aggiornamento dati in tempo reale delle timbrature
- Accesso a dati via Web e Mobile
- Modifica dei dati delle timbrature
- Gestione entrate e uscite fuori orario
- Gestione giustificazioni
- Storico assenze per alunno
- Gestione e assegnazione dei badges
- Notifiche Telegram al genitore

**Da valutare in future versioni:**

- Importazione CSV per integrazione con ARGO
- Connessione Arduino con Wi-Fi e rete GSM
- Migliore gestione degli account
- Storico anni precedenti

**Nice to Have:**

- Riscontro acustico di avvenuta timbratura e/o errore
- Notifica se numero di assenze > X
- App iOS
- Memorizzazione d'emergenza dati timbrature nella scheda sd dell'arduino
- App per timbratura attraverso scansione QRcode
- App Docenti a pari funzionalità della versione web
- Gestione orario docenti
- Notifiche al genitore anche via e-mail

<div class="page-break" />

## **Materiali e risorse utilizzate**
![](https://s4.postimg.org/dcenywml9/Stud_Entry.png)

#### **Hardware**
- Arduino Uno Rev3
- Arduino Ethernet Shield
- Card reader RFID MFRC522 a 13.56MHz
- Cards RFID/NFC MiFare 13.56MHz
- Buzzer
- Raspberry Pi 3
- Switch 5 Porte RJ45 Fast Ethernet 10/100 Mbps
- Access Point (realmente è utilizzato un router D-Link opportunamente configurato)
- Cavi patch Ethernet di rete Cat6 con connettori RJ45

#### **Software (librerie esterne)**
- Bootstrap
- Font Awesome Icon
- Php Serial
- JQuery Circle Progress
- JQuery Loader
- JQuery Timeline
- JavaScript Cookie

#### **Linguaggi**
- HTML
- CSS
- PHP
- MySQL
- JQuery
- Android
- Arduino

#### **Coordinamento del gruppo**
- Slack (piattaforma di comunicazione)
- Repository BitBucket (client git)
- Riunioni regolari in base agli impegni scolastici

#### **Studio e risoluzione dei problemi**
- Documentazioni ufficiali online dei vari linguaggi utilizzati
- W3Schools e altri siti di tutoraggio, esemplificazione del codice, documentazione, adattando le spiegazioni generali agli obiettivi specifici del progetto
- StackOverflow e altri forum online per la risoluzione delle varie problematiche riscontrate
- Consultazione con alcuni professori interni alla classe

<div class="page-break" />

## **Database**
La progettazione del database è stata redatta dopo un'accurata analisi della realtà di riferimento ed una profonda discussione riguardante le possibili soluzioni di realizzazione e le molteplici opzioni di implementazione, trascrivendo le nozioni principali e i dettagli più utili per la successiva fase di progettazione concettuale tramite diagramma E-R.
Dopo ulteriori considerazioni sono state evidenziate le entità (con i loro attributi) e le relazioni che le legano fra di esse, concretamente definite nello schema relazionale creato inizialmente su carta e digitalizzato in seguito.

![](https://s15.postimg.org/5zr0pqa0r/erdef.png)

***Descrizione struttura database***

Dal contesto in questione l'entità che è emersa per prima è naturalmente quella del ALUNNO, i cui attributi sono stati scelti maggiormente per essenzialità e comodità di amministrazione, dato che la scelta degli stessi è stata del tutto personale poiché la comunicazione con il portale Argo non si è potuta instaurare, dunque non potendo usufruire di quelli predefiniti da tale servizio scolastico.
Gli alunni vengono memorizzati nel database tramite un id, un solo dato anagrafico ovvero il nome (per comodità e scarsa utilità, al momento, di ulteriori dati essendo già presenti altre informazioni che permettono il riconoscimento univoco dell'alunno nell'applicativo) e un 'numerotessera', ovvero il numero che identifica la tessera personale (RFID card) assegnatagli ad inizio anno, che grazie alle sue timbrature sul dispositivo Studentry posto all'entrata della scuola segnalerà gli spostamenti dell'alunno da esterno a interno e viceversa.
Telegram_username e chat_id, ovvero le credenziali di accesso a Telegram del genitore, sono state memorizzate direttamente come campi di questa tabella senza dover creare una tabella genitore a parte legata a quella dell'alunno tramite una relazione uno a uno, che pareva più futile e scomodo in confronto alla scelta messa in atto anche per motivi di rapidità.

L'entità ALUNNO è legata tramite una relazione 1 a molti all'entità ASSENZA, di cui vengono registrati un id e un attributo 'giustificata' che
possiede come valore la data della giustificazione oppure valore nullo nel caso essa non sia stata ancora giustificata.

Un'entità CALENDARIO (rappresentante il vero e proprio calendario annuale) è poi legata a questa attraverso una relazione 1 a molti, di questa entità vengono memorizzati gli attributi id, 'giorno' (ovvero ogni singolo giorno dell'anno rappresentato dalla sua data) e 'valore' che lo descrive come feriale o festivo.

L'entità ALUNNO dall'altro lato è legata mediante una relazione 1 a molti all'entità REGISTRAZIONE (rappresentate ogni singola giornata scolastica a cui l'alunno partecipa) che comprende un id, la data, l'ora di entrata e l'ora di uscita come suoi attributi, assieme al campo 'approvata' che assume valore vero nel caso sia stata approvata dalla segreteria oppure falso in caso contrario.
Quest'ultima entità è poi legata per mezzo di una relazione 1 a molti all'entità USCITA, avente come attributi id e ora di uscita.

L'entità ALUNNO è inoltre legata all'entità CLASSE (avente solamente gli attributi id e 'nome' che la identificano univocamente) tramite una relazione 1 a molti. Quest'ultima è poi legata all'entità LEZIONE (identificata solamente con il suo id) attraverso una relazione 1 a molti, questa entità è direttamente collegata mediante una relazione 1 a molti all'entità DOCENTE che viene identificato tramite un id, un solo dato anagrafico ovvero il nome (secondo il ragionamento fatto precedentemente per la tabella del alunno) e le credenziali di registrazione (username e password) agli applicativi web e mobile di Studentry.

L'entità LEZIONE è inoltre legata grazie ad una relazione 1 a molti all'entità ORARIO che possiede un id come suo unico attributo ed è a sua volta legata attraverso una relazione 1 a molti all'entità ORA che ha come attributi un id, 'data_inizio', 'data_fine' e 'nome_ora'.

Vi è infine un'entità GIORNO, seperata dalle altre entità, creata solamente per memorizzare il nome dei giorni della settimana, quindi avente solamente sei istanze al suo interno che corrispondono ai giorni scolastici effettivi senza la domenica. Oltre a questa vi è un'altra entità separata dal resto che è quella dell'UTENTE_SEGRETERIA, tabella (in questa versione dell'app non necessita di associazioni con altre entità) di stampo identica a quella del docente , in cui vengono memorizzati gli utenti del personale della segreteria.

***Note particolari***

E' stato scelto di creare una tabella unica per le registrazioni al posto che due separate per entrate ed uscite, risparmiando notevolmente sulla semplicità di realizzazione e gestione ma soprattutto sulla memorizzazione di tutti i dati delle timbrature degli alunni, nella tabella creatasi il campo fk_registrazione li contiene tutti al suo interno.

<div class="page-break" />

## **Arduino**

L'ambiente di sviluppo integrato (IDE) non era sconosciuto. Arduino infatti era stato affrontato durante alcune ore della materia "Scienze e Tecnologie Applicate" durante il secondo anno in quest'istituto. Si conoscevano quindi le funzioni `void setup()` e `void loop()` con la loro relativa logica di utilizzo e dei piccoli accenni sulla struttura base del codice. Il linguaggio di sviluppo con cui vengono scritti i programmi di Arduino deriva dal C/C++, linguaggio affrontato durante il terzo e quarto anno. Tutto ciò ha permesso di limitare il tempo dedicato allo studio teorico e di concentrarsi principalmente sullo studio delle librerie utilizzate, sfruttando anche la filosofia Open Hardware e Open Source che ha facilitato le operazioni di studio e scrittura del sorgente, grazie anche ai molti esempi forniti con le librerie e a quelli disponibili online.

Gli scatch pensati in fase di progettazione sono due:
- Scatch timbratura alunni
- Scatch per l'assegnazione dei badges agli alunni

#### **Scatch timbratura alunni**

L'Arduino che registra le timbrature giornaliere degli studenti comunica con il resto dell'applicativo attraverso la rete e sarà composto da:
- (Arduino)
- Ethernet Shield
- Card reader RFID
- Buzzer

L'Ethernet Shield prova prima a connettersi utilizzando il DHCP Server, se questa configurazione fallisce effettuerà un tentativo di connessione utiizzando i parametri statici peresenti nel codice. Per la connessione con DHCP è inoltre prevista la funzione `void maintain()` che si occuperà di gestire la configurazione dinamica dell'indirizzo IP ed eventuali altri parametri. Per l'implementazione del codice necessario alla connessione è stata utilizzata la libreria `Ethernet.h`.
Per il rilevamento delle timbrature, che avviene attraverso il Card reader RFID, è stata invece utilizzata la libreria .
Una volta letto il numero della carta, l'Arduino invia questo numero, attraverso una richiesta GET, ad una pagina PHP, che effettuerà le operazioni di registrazione.
All'Arduino è collegato anche un Buzzer che restituisce un feedback sulla lettura della carta.

**N.B.** Il Buzzer segnala l'avvenuta lettura della carta, non l'effettiva memorizzazione della timbratura. Per questa funzionalità è necessario utilizzare la libreria `MySqlConnector.h` come spiegato nella sezione *Sviluppi futuri*.

#### **Scatch assegnazione badge**
La segreteria a inizio anno dovrà assegnare ad ogni alunno un badge. Quest'operazione può essere effettuata con un Arduino appositamente programmato, connesso alla porta USB del computer.
L'Arduino non si quindi connette alla rete e monta i seguenti componenti:
- (Arduino)
- Card reader RFID
- Buzzer

Il numero letto dal Card reader RFID viene stampato sulla porta seriale da dove viene prelevato da un'apposita pagina PHP che si occuperà di associare la carta all'alunno all'interno della base dati.
Anche qui è stato previsto un Buzzer per un feedback di avvenuta lettura (non memorizzazione).

<div class="page-break" />

## **Applicativo Web**

L'applicativo web, sviluppato con l'utilizzo del Framework Bootstrap per garantirne la responsività grafica, mira a favorire l'esperienza utente, anche attraverso alcune semplici animazioni ed è stato pensato in modo da rendere il suo utilizzo il più intuibile possibile.
Essendo che StudEntry è un gestionale, si è cercato di sviluppare l'applicativo web-based sottoforma di Single-page application (SPA), con l'intento di simulare maggiormente il funzionamento dei gestionali desktop-based. Per raggiungere tale scopo è stato fatto uso del Framework JavaScript JQuery gestendo le chiamate asincorne della SPA con Ajax. Nella versione attuale dell'applicativo, questa funzione è stata integrata includendo gli eventuali dati ritornati dalla chiamata asincrona direttamente con codice HTML nella pagina principale. Uno *Sviluppo Futuro* è utilizzare il formato JSON per ritornare questi dati.

###### **Esempio di struttura**
Si riporta l'esempio della pagina principale dell'interfaccia riservata ai Docenti:

![appweb_homedocenti](https://s12.postimg.org/7tug01ahp/Untitled_Diagram.png)

Come si può notare la pagina Home Docenti è in realtà una composizione di altre pagine, quali `script/elencoclassi.php`, `script/presenti.php` e `script/assenti.php`.
L'utente accederà esclusivamente la pagina `docenti/index.php`, ma il contenuto di questa cambierà ugualmente a seconda delle azioni svolte, senza influire sull'esperienza di utilizzo. Ad esempio, cliccando sul bottone Giustificazioni, al posto della pagina `strutture/home.php` verrà caricata la pagina `strutture/giustificazioni.php` con i suoi relativi dati e script.

<div class="page-break" />


## **App Android**

L'applicativo android è stato sviluppato con android studio, un software gratuito che comprende al suo interno tutti gli strumenti necessari per lo sviluppo.La scelta di utilizzare questo software è stata pressochè immediata essendo il più utilizzato e il più comodo; inoltre sono stati sviluppati degli script lato server per interfacciare l'applicazione con il server.              
I linguaggi di programmazione che sono stati utilizzati sono:

- Java (connessione e logica dell'applicazione)
- Xml  (layout dell'applicazione)
- Php  (script lato server)

#### **Problematiche iniziali**

I primi passi intrapresi sono stati molto difficili, infatti dovendo usare internet come sola fonte di consultazione e non avendo un docente di riferimento per i problemi specifici  incontrati, sono state trovate metodologie discordanti tra di loro, o metodi di sviluppo ormai deprecati.
Un altro problema che è stato riscontrato è dovuto alla mancanza di esperienza che non  ha permesso di determinare delle tempistiche più o meno precise per lo sviluppo delle varie fasi del sottoprogetto.
La soluzione a queste problematiche si è ritrovata principalmente nella consultazione di forum online, ma anche attraverso il confronto con i compagni di classe che stavano svolgendo progetti nello stesso ambito.

#### **Concezione e Progettazione**
 La fase di concezione e progettazione è stata svolta a livello di gruppo, decidendo come uniformare le funzionalità dell'applicazione mettendole in confronto con i servizi forniti dal sito web.
 Anche in questo caso è stato fondamentale il ruolo di internet che ha fornito le basi e le linee guida per la definizione della base del progetto. Sono state definite inoltre in fase di progettazione delle tempistiche per la conclusione del progetto.

####  **Realizzazione**
La fase di realizzazione è stata soggetta a ritardi imprevisti già nelle prime fasi, infatti come anticipato in precedenza la mancanza di esperienza ha portato alla realizzazione di classi non efficaci, e l'utilizzo di metodi non deprecati, ma fortemente in disuso. Questo ha comportato continue modifiche del progetto iniziale e lo sviluppo ripetuto di alcune parti dell'applicazione.
##### Scelte di sviluppo e motivazioni
Le scelte più marcanti all'interno del progetto hanno riguardato:
###### - L'utilizzo di volley
Volley è una libreria di Google che fornisce metodi e classi per il networking.
Il suo utilizzo, anche se non obbligatorio, porta molti vantaggi in quanto evita l'utilizzo della classe java.net.HttpURLConnection che oltre a costringere all'uso della classe Asynctask per gestire le richieste, è molto più lenta di Volley nel fare le richieste.
I principali punti a favore sono:
-  metodi che garantiscono una  maggior velocità di trasmissione
-  caching, se un activity viene distrutta tutti i dati sono salvati   temporaneamente
-  si occupa automaticamente delle transizioni HTTP
-  gestisce automaticamente gli errori di rete

![](https://s16.postimg.org/dola95iqt/volley.jpg)

###### - L'utilizzo di CardView e RecycleView

Recycleview è un widget che viene utilizzato quando i dati forniti dall'applicativo dipendono da azioni dell'utente o dalla rete.Questo componente  agisce da contenitore per grandi DataSet; gestisce inoltre la posizione dei componenti grafici al suo interno tramite l'uso di un LayoutManager e gestisce efficientemente i dati attraverso l'utiizzo di una o più classi adapter.

![](https://s17.postimg.org/uh5f1la5b/Recycler_View.png)

Cardview estende la classe FrameLayout;  permette di visuallizzare dati, immagini, menu ed altri elementi all'interno di una carta. Esso è molto utilizzato attualmente in quanto garantisce la responsività, ed è di facile utilizzo. Viene utilizzato assieme alla RecycleView fornendo ottime prestazioni.

####  **Funzionalità e funzionamento dell'applicazione**

L'applicazione permette ai docenti di visualizzare gli alunni presenti quelli assenti e di gestire entrate o uscite fuori orario. Per quanto riguarda la parte logica seppur funzionante, necessità di un'ulteriore fase di test e debugging. La parte grafica  è basilare; i componenti grafici utilizzati  più che all'apparenza mirano alla responsività.

<div class="page-break" />

## **Bot Telegram**

E' stato scelto di utilizzare il servizio di chat gratuito Telegram per la creazione di un bot che invia automaticamente un messaggio ai genitori degli alunni che rimangono assenti dalle lezioni, mentre quando lo studente desidera uscire anticipatamente dall'edificio scolastico, nel caso non sia stato effettuato il permesso telefonico del genitore, quest'ultimo, rispondendo al bot che gli notificherà la richiesta di uscita del figlio, potrà approvarla direttamente dalla chat, consentendo dunque al figlio di timbrare l'uscita dalla scuola che altrimenti non sarà accettata.

## **Sviluppi futuri**

Principalmente per motivi di tempo non è stato possibile inserire alcune funzionalità, pensate all'inizio o nate durante lo sviluppo, che potranno essere integrate nell'applicativo StudEntry per migliorarlo ed avvalorarlo ulteriormente. Tali funzionalità sono descritte di seguito:

- Invio diretto delle query dall'arduino al database attraverso la libreria `MySqlConnector.h` , mantenendole invariate ma potendo gestire i controlli sul loro stato di esecuzione, in modo da comunicare a chi effettua la timbratura un feedback più appropriato, immediato e corretto attraverso il Buzzer piuttosto che gli attuali messaggi stampati a video che l'Arduino non è programmato per leggere.

- Memorizzazione dei dati delle timbrature all'interno di una scheda SD inserita nell'arduino cosìcché, nel caso si presentino guasti di svariata natura, sia comunque possibile per il personale della segreteria reperire i dati delle timbrature in un comodo file da caricare in StudEntry manualmente per aggiornare i dati.

- Scrittura di un'applicazione mobile che l'alunno può utilizzare per effettuare la registrazione (timbratura) tramite la scansione di un QRcode che si collega alla stessa pagina php attualmente in uso per la memorizzazione delle registrazioni nel database, creando dunque una valida alternativa rispetto alla metodologia messa in pratica al momento.

- Creazione di un'interfaccia per la segreteria che permetta di caricare un file in formato CSV opportunamente scaricato dal portale ARGO, contentente tutti i dati necessari per il funzionamento di StudEntry, in modo da facilitare e non ripetere le operazioni di configurazione a inizio dell'anno scolastico, permettendo dunque di utilizzare quelle già utilizzate nel registro elettronico.

- Utilizzo di una tabella apposita (frutto di una query di selezione), già presente nel database, formata da FK_docente, FK_classe, FK_ora e FK_giorno per la  comodità di gestione delle lezioni di ogni singolo docente. L'idea è di recuperare questi dati in modo che ogni docente possa visualizzare il suo orario di insegnamento giornaliero e in qualsiasi momento possa organizzare, in modo più immediato ed intuitivo, i suoi "movimenti scolastici" in base a spazio e tempo. Questa operazione potrà anche venire effettuata dagli utenti della segreteria.

## **Conclusioni finali**

Per concludere si può dire che il lavoro effettuato è nel complesso soddisfacente. Nonostante le varie problematiche di tempo, di gestione e imprevisti vari, è stata portata  a termine la base del progetto. Alcune migliorie che possono essere apportate al progetto sono già state pensate e sono descritte nella sezione *Sviluppi Futuri*. Il progetto ha fornito al gruppo l'esperienza sulla gestione e sullo sviluppo di un progetto informatico, sollevando problematiche e necessità che si possono incontrare in una realtà lavorativa ed ha permesso inoltre di creare interdisciplinarietà tra le materie tecniche studiate a scuola approfondendo le conoscenze acquisite durante l'anno scolastico, rappresentando anche un punto di spinta per lo studio di nuovi elementi (linguaggi, tecniche, concetti, ...) non affrontati nei programmi didattici.
